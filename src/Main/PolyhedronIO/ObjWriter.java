package PolyhedronIO;

import de.javagl.obj.Obj;
import de.javagl.obj.Objs;

import javax.vecmath.Point3d;
import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A helper class to write Obj files that contain information for unfoldable polyhedra.
 */
class ObjWriter {

	/**
	 * Tolerance for rounding coordinate values.
	 */
	private static double TOL = 1e-5;

	/**
	 * Factor used in rounding calculation.
	 */
	private static double FACTOR = Math.round(1.0/TOL);

	/**
	 * Converts face colours to a string.
	 * @param colours  The colours.
	 * @param numFaces The number of faces.
	 * @return The string.
	 */
	static String writeFaceColours(Color[] colours, int numFaces) {

		StringBuilder sb = new StringBuilder("");
		if (colours == null) return sb.toString();

		for (int i = 0; i < numFaces; i++) {
			Color c = colours[i];
			if (c == null) continue;
			sb.append("\nfc ");
			sb.append((i+1));
			sb.append(" ");
			sb.append(c.getRed());
			sb.append(" ");
			sb.append(c.getGreen());
			sb.append(" ");
			sb.append(c.getBlue());
			sb.append((c.getAlpha() == 255) ? "" : (" " + c.getAlpha()));
		}
		return sb.toString();
	}

	/**
	 * Converts a polyhedron to an Obj object.
	 * @param polyhedron The polyhedron.
	 * @return The Obj object.
	 */
	static Obj polyhedronToObj(Polyhedron.IPolyhedron polyhedron) {
		Obj obj = Objs.create();
		for (Point3d v : polyhedron.getVertices()) {
			obj.addVertex(
				(float) (Math.round(v.x * FACTOR) / FACTOR),
				(float) (Math.round(v.y * FACTOR) / FACTOR),
				(float) (Math.round(v.z * FACTOR) / FACTOR)
			);
		}
		for (int[] ei : polyhedron.getEdgeIndices()) {
			obj.addFace(ei);
		}
		for (int[] fi : polyhedron.getFaceIndices()) {
			obj.addFace(fi);
		}
		return obj;
	}

	/**
	 * Replaces lines of the form "f val1 val2" with "l val1 val2" since they represent
	 * lines instead of faces. This is necessary because the Obj parser cannot handle
	 * line inputs.
	 * @param s The string to replace.
	 * @return The replaced string.
	 */
	static String replaceTwoElementFaceIndsWithLineInds(String s) {
		Pattern p = Pattern.compile("f \\d+ \\d+");
		String lines[] = s.split("\\r?\\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			Matcher m = p.matcher(line);
			if (m.matches()) {
				String newLine = line.replaceFirst("f", "l");
				lines[i] = newLine;
			}
		}
		return String.join("\n", lines);
	}
}
