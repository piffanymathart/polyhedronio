package PolyhedronIO;

import Polyhedron.ColouredPolyhedron;
import Polyhedron.IColouredPolyhedron;
import Unfolding.INode;
import Unfolding.Node;
import de.javagl.obj.FloatTuple;
import de.javagl.obj.Obj;
import de.javagl.obj.ObjFace;

import javax.vecmath.Point3d;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A helper class to read Obj files that contain information for unfoldable polyhedra.
 */
class ObjReader {

	/**
	 * Converts a OBJ file string into a polyhedron.
	 *
	 * @param s The OBJ file string.
	 * @return The polyhedron.
	 * @throws IOException If OBJ file reading fails.
	 */
	static IColouredPolyhedron parsePolyhedron(String s) throws IOException {

		InputStream inputStream = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8.name()));
		Obj obj = de.javagl.obj.ObjReader.read(inputStream);

		validateFaces(obj);

		Point3d[] vertices = getVertices(obj);
		int[][] edgeIndices = readLineIndices(s);
		int[][] faceIndices = getFaceIndices(obj);
		Color[] faceColours = readFaceColours(s, faceIndices.length);

		return new ColouredPolyhedron(vertices, edgeIndices, faceIndices, faceColours);
	}

	/**
	 * Validate whether each face have at least 3 vertices.
	 * @param obj The Obj object.
	 * @throws RuntimeException If a face has fewer than 3 vertices.
	 */
	private static void validateFaces(Obj obj) {
		int n = obj.getNumFaces();
		for (int i = 0; i < n; i++) {
			if (obj.getFace(i).getNumVertices() < 3) {
				throw new RuntimeException("Obj file must contain only faces with at least 3 vertices.");
			}
		}
	}

	/**
	 * Reads the face colour from a set of tokens representing greyscale value, RGB, or RGBA.
	 * @param tokens Tokens containing colour information.
	 * @return The face colour.
	 * @throws RuntimeException If the tokens are invalid.
	 */
	private static Color readFaceColour(String tokens[]) {
		int n = tokens.length;
		try {
			switch (n) {
				case 1: {
					int gray = Integer.valueOf(tokens[0]);
					return new Color(gray, gray, gray);
				}
				case 3: {
					int red = Integer.valueOf(tokens[0]);
					int green = Integer.valueOf(tokens[1]);
					int blue = Integer.valueOf(tokens[2]);
					return new Color(red, green, blue);
				}
				case 4: {
					int red = Integer.valueOf(tokens[0]);
					int green = Integer.valueOf(tokens[1]);
					int blue = Integer.valueOf(tokens[2]);
					int alpha = Integer.valueOf(tokens[3]);
					return new Color(red, green, blue, alpha);
				}
				default:
					throw new RuntimeException("Input file specified invalid colours.");
			}
		} catch (Exception e) {
			throw new RuntimeException("Input file specified invalid colours.");
		}
	}

	/**
	 * Reads the face colours from a string.
	 * @param s The string.
	 * @param numFaces The number of faces.
	 * @return The face colours.
	 * @throws RuntimeException If the string is invalid.
	 */
	private static Color[] readFaceColours(String s, int numFaces) {
		Color[] faceColours = new Color[numFaces];

		String lines[] = s.split("\\r?\\n");
		for (String line : lines) {
			if (line.startsWith("fc ")) {
				line = line.substring(3).trim();
				String[] tokens = line.split("\\s+");
				int n = tokens.length;
				if (n < 1) continue;
				int faceInd;
				try {
					faceInd = Integer.valueOf(tokens[0]) - 1;
				} catch (Exception e) {
					throw new RuntimeException("Input file specified invalid face indices for face colours.");
				}
				if (faceInd < 0 || faceInd >= faceColours.length) {
					throw new RuntimeException("Input file specified invalid face indices for face colours.");
				}
				Color c = readFaceColour(Arrays.copyOfRange(tokens, 1, tokens.length));
				faceColours[faceInd] = c;
			}
		}

		return faceColours;
	}

	/**
	 * Reads the line indices from a string, since the Obj reader does not support line reading.
	 * @param s The string.
	 * @return The line indices.
	 * @throws RuntimeException If the string is invalid.
	 */
	private static int[][] readLineIndices(String s) {
		List<int[]> lineInds = new ArrayList<>();
		String lines[] = s.split("\\r?\\n");
		for (String line : lines) {
			if (line.startsWith("l ")) {
				line = line.substring(2).trim();
				String[] tokens = line.split("\\s+");
				int n = tokens.length;
				if (n < 2) throw new RuntimeException("Input file specified invalid edges.");
				try {
					int[] vals = new int[n];
					for (int i = 0; i < n; i++) vals[i] = Integer.valueOf(tokens[i]) - 1;
					for (int i = 1; i < n; i++) lineInds.add(new int[]{vals[i - 1], vals[i]});
				} catch (Exception e) {
					throw new RuntimeException("Input file specified invalid edges.");
				}
			}
		}
		return lineInds.toArray(new int[lineInds.size()][]);
	}

	/**
	 * Parses a string into a tree node.
	 * @param s The string.
	 * @return The parsed node, or null if unsuccessful.
	 */
	static INode parseTree(String s) {
		String lines[] = s.split("\\r?\\n");
		for (String line : lines) {
			if (line.startsWith("net ")) {
				return new Node(line.replaceFirst("net ", ""));
			}
		}
		return null;
	}

	/**
	 * Gets the vertices from an Obj object.
	 * @param obj The Obj object.
	 * @return The vertices.
	 */
	private static Point3d[] getVertices(Obj obj) {
		int n = obj.getNumVertices();
		Point3d[] vertices = new Point3d[n];
		for (int i = 0; i < n; i++) {
			FloatTuple t = obj.getVertex(i);
			vertices[i] = new Point3d(t.getX(), t.getY(), t.getZ());
		}
		return vertices;
	}

	/**
	 * Gets the face indices from an Obj object.
	 * @param obj The Obj object.
	 * @return The face indices.
	 */
	private static int[][] getFaceIndices(Obj obj) {
		int n = obj.getNumFaces();
		List<int[]> faceIndices = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			ObjFace f = obj.getFace(i);
			int m = f.getNumVertices();
			if (m > 2) {
				int[] inds = new int[m];
				for (int j = 0; j < m; j++) inds[j] = f.getVertexIndex(j);
				faceIndices.add(inds);
			}
		}
		return faceIndices.toArray(new int[faceIndices.size()][]);
	}
}
