package PolyhedronIO;

import Polyhedron.ColouredPolyhedron;
import Polyhedron.IColouredPolyhedron;
import Unfolding.INode;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.UnfoldablePolyhedron;
import de.javagl.obj.Obj;

import javax.vecmath.Point3d;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * A helper class for reading and writing polyhedra.
 */
public interface PolyhedronIO {

	/**
	 * Converts a string into an unfoldable polyhedron.
	 * @param s The text to parse.
	 * @return The unfoldable polyhedron.
	 * @throws IOException If file reading fails.
	 */
	static IUnfoldablePolyhedron read(String s) throws IOException {
		IColouredPolyhedron polyhedron = ObjReader.parsePolyhedron(s);
		INode tree = ObjReader.parseTree(s);
		return new UnfoldablePolyhedron(polyhedron, tree);
	}

	/**
	 * Converts an unfoldable polyhedron to a string.
	 * @param unfoldablePolyhedron The unfoldable polyhedron.
	 * @return The string representation.
	 * @throws IOException If file writing fails.
	 */
	static String write(IUnfoldablePolyhedron unfoldablePolyhedron) throws IOException {

		OutputStream outputStream = new ByteArrayOutputStream();
		Obj obj = ObjWriter.polyhedronToObj(unfoldablePolyhedron);
		de.javagl.obj.ObjWriter.write(obj, outputStream);
		StringBuilder sb = new StringBuilder(outputStream.toString());

		INode tree = unfoldablePolyhedron.getNetTree();
		if(tree != null) {
			sb.append("# The tree structure specifying the polyhedral net unfolding\n");
			sb.append("net ");
			sb.append(tree.toString());
		}

		sb = new StringBuilder(ObjWriter.replaceTwoElementFaceIndsWithLineInds(sb.toString()));
		sb.append("\n");
		sb.append(ObjWriter.writeFaceColours(unfoldablePolyhedron.getFaceColours(), unfoldablePolyhedron.getNumFaces()));

		return sb.toString();
	}

	/**
	 * Reads an image from file and converts it into a polyhedron.
	 * @param img The image.
	 * @return A polyhedron representing a 1x1 image centred at the origin, with coloured
	 * faces as pixels.
	 * @throws IOException If image reading fails.
	 */
	static IColouredPolyhedron read(BufferedImage img) throws IOException {

		int w = img.getWidth();
		int h = img.getHeight();

		Point3d[] vertices = new Point3d[(w+1)*(h+1)];
		for(int i = 0; i <= w; i++) {
			for (int j = 0; j <= h; j++) {
				double x = (double)i / (double)w - 0.5;
				double y = - ((double)j / (double)h - 0.5);
				vertices[Helper.get1dIndex(i,j,h)] = new Point3d(x,y,0);
			}
		}

		int[][] faceIndices = new int[w*h][4];
		Color[] faceColours = new Color[w*h];
		int indexOfFaceInd = 0;
		for(int i=0; i<w; i++) {
			for(int j=0; j<h; j++) {
				int[] faceInd = {
					Helper.get1dIndex(i,j,h),
					Helper.get1dIndex(i+1,j,h),
					Helper.get1dIndex(i+1,j+1,h),
					Helper.get1dIndex(i,j+1,h)
				};
				faceIndices[indexOfFaceInd] = faceInd;
				faceColours[indexOfFaceInd] = new Color(img.getRGB(i,j));
				indexOfFaceInd++;
			}
		}

		return new ColouredPolyhedron(vertices, null, faceIndices, faceColours);
	}

	class Helper {
		/**
		 * Gets the 1D index from a 2D index.
		 * @param i The first index.
		 * @param j The second index.
		 * @param maxJ The maximum value of the second index.
		 * @return The corresponding 2D index.
		 */
		private static int get1dIndex(int i, int j, int maxJ) {
			return i * (maxJ + 1) + j;
		}
	}
}
