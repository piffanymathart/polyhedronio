package PolyhedronIO;

import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;
import Polyhedron.IColouredPolyhedron;
import Unfolding.INode;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.Node;
import Unfolding.UnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PolyhedronIOTest {

	@Test
	void readWrite_unfoldablePolyhedron() throws IOException {

		Point3d[] verts = {
			new Point3d(0,0,0),
			new Point3d(0,1,1),
			new Point3d(1,1,2),
			new Point3d(1,0,1)
		};
		int[][] edgeInds = {{3,0}, {1,2}};
		int[][] faceInds = {{0,1,2,3},{0,3,2},{1,0,3},{1,3,2}};

		Color[] faceColours = {new Color(1,2,3), null, new Color(4,5,6,7), null};

		INode tree = new Node("0(2,1)");

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(verts, edgeInds, faceInds, faceColours, tree);

		String s = PolyhedronIO.write(polyhedron);
		IUnfoldablePolyhedron newPolyhedron = PolyhedronIO.read(s);
		assertEquals(polyhedron, newPolyhedron);
	}

	@Test
	void read_polyhedron() throws IOException {

		Point3d p1 = new Point3d(11,12,13);
		Point3d p2 = new Point3d(21,22,23);
		Point3d p3 = new Point3d(31,32,33);
		Point3d p4 = new Point3d(41,42,43);

		String s = "v 11.0 12.0 13.0\n" +
			"v 21.0 22.0 23.0\n" +
			"v 31.0 32.0 33.0\n" +
			"v 41.0 42.0 43.0\n" +
			"l 4 1\n" +
			"l 2 3\n" +
			"l 2 1 4 3\n" +
			"f 3 2 1\n" +
			"f 2 1 4\n" +
			"f 1 2 3 4\n" +
			"f 4 3 1\n" +
			"fc 1 10\n" +
			"fc 2 10 20 30\n" +
			"fc 4 10 20 30 40\n";

		IColouredPolyhedron newPolyhedron = PolyhedronIO.read(s);

		assertArrayEquals(
			new Line3d[] {
				new Line3d(p4,p1),
				new Line3d(p2,p3),
				new Line3d(p2,p1),
				new Line3d(p1,p4),
				new Line3d(p4,p3)
			},
			newPolyhedron.getEdges()
		);

		assertArrayEquals(
			new int[][] {
				{3,0},
				{1,2},
				{1,0},{0,3},{3,2}
			},
			newPolyhedron.getEdgeIndices()
		);

		assertArrayEquals(
			new int[][] {
				{2,1,0},
				{1,0,3},
				{0,1,2,3},
				{3,2,0}
			},
			newPolyhedron.getFaceIndices()
		);

		assertArrayEquals(
			new Color[] {
				new Color(10,10,10),
				new Color(10,20,30),
				null,
				new Color(10,20,30,40)
			},
			newPolyhedron.getFaceColours()
		);
	}

	@Test
	void read_polyhedron_invalidEdge() {

		String s = "v 11.0 12.0 13.0\n" +
			"v 21.0 22.0 23.0\n" +
			"v 31.0 32.0 33.0\n" +
			"v 41.0 42.0 43.0\n" +
			"l 4 1\n" +
			"l s 3\n" +
			"l 2 1 4 3\n" +
			"f 3 2 1\n" +
			"f 2 1 4\n" +
			"f 1 2 3 4\n" +
			"f 4 1 3\n" +
			"fc 1 10\n" +
			"fc 2 10 20 30\n" +
			"fc 4 10 20 30 40\n";

		assertThrows(RuntimeException.class, () -> PolyhedronIO.read(s));
	}

	@Test
	void read_polyhedron_invalidFaceIndex() {

		String s = "v 11.0 12.0 13.0\n" +
			"v 21.0 22.0 23.0\n" +
			"v 31.0 32.0 33.0\n" +
			"v 41.0 42.0 43.0\n" +
			"l 4 1\n" +
			"l 2 3\n" +
			"l 2 1 4 3\n" +
			"f 3 2 1\n" +
			"f 2 1 4\n" +
			"f 1 2 3 4\n" +
			"f 4 3 1\n" +
			"fc 6 10\n" +
			"fc 2 10 20 30\n" +
			"fc 4 10 20 30 40\n";

		assertThrows(RuntimeException.class, () -> PolyhedronIO.read(s));
	}

	@Test
	void read_polyhedron_faceHasTooFewVertices() {

		String s = "v 11.0 12.0 13.0\n" +
			"v 21.0 22.0 23.0\n" +
			"v 31.0 32.0 33.0\n" +
			"v 41.0 42.0 43.0\n" +
			"l 4 1\n" +
			"l 2 3\n" +
			"l 2 1 4 3\n" +
			"f 3 2 1\n" +
			"f 2 1 4\n" +
			"f 1 2 3 4\n" +
			"f 4 1\n" +
			"fc 1 10\n" +
			"fc 2 10 20 30\n" +
			"fc 4 10 20 30 40\n";

		assertThrows(RuntimeException.class, () -> PolyhedronIO.read(s));
	}

	@Test
	void read_polyhedron_invalidColor1() {

		String s = "v 11.0 12.0 13.0\n" +
			"v 21.0 22.0 23.0\n" +
			"v 31.0 32.0 33.0\n" +
			"v 41.0 42.0 43.0\n" +
			"l 4 1\n" +
			"l 2 3\n" +
			"l 2 1 4 3\n" +
			"f 3 2 1\n" +
			"f 2 1 4\n" +
			"f 1 2 3 4\n" +
			"f 4 3 1\n" +
			"fc 1 10 20\n" +
			"fc 2 10 20 30\n" +
			"fc 4 10 20 30 40\n";

		assertThrows(RuntimeException.class, () -> PolyhedronIO.read(s));
	}

	@Test
	void read_polyhedron_invalidColor2() {

		String s = "v 11.0 12.0 13.0\n" +
			"v 21.0 22.0 23.0\n" +
			"v 31.0 32.0 33.0\n" +
			"v 41.0 42.0 43.0\n" +
			"l 4 1\n" +
			"l 2 3\n" +
			"l 2 1 4 3\n" +
			"f 3 2 1\n" +
			"f 2 1 4\n" +
			"f 1 2 3 4\n" +
			"f 4 3 1\n" +
			"fc 1 10 20\n" +
			"fc 7 10 20 30\n" +
			"fc 4 10 20 30 40\n";

		assertThrows(RuntimeException.class, () -> PolyhedronIO.read(s));
	}

	@Test
	void read_polyhedron_invalidColor3() {

		String s = "v 11.0 12.0 13.0\n" +
			"v 21.0 22.0 23.0\n" +
			"v 31.0 32.0 33.0\n" +
			"v 41.0 42.0 43.0\n" +
			"l 4 1\n" +
			"l 2 3\n" +
			"l 2 1 4 3\n" +
			"f 3 2 1\n" +
			"f 2 1 4\n" +
			"f 1 2 3 4\n" +
			"f 4 3 1\n" +
			"fc s 10 20 30\n" +
			"fc 4 10 20 30 40\n";

		assertThrows(RuntimeException.class, () -> PolyhedronIO.read(s));
	}

	@Test
	void readwrite() throws IOException {

		int w = 2;
		int h = 2;
		BufferedImage img = new BufferedImage(w, h,	BufferedImage.TYPE_INT_RGB);
		for(int i=0; i<w; i++) {
			for(int j=0; j<h; j++) {
				Color c = new Color(i, j, i+j);
				img.setRGB(i, j, c.getRGB());
			}
		}

		IColouredPolyhedron polyhedron = PolyhedronIO.read(img);

		IPolygon3d[] faces = polyhedron.getFaces();
		Color[] faceColours = polyhedron.getFaceColours();
		assertEquals(4, faces.length);

		// 0 = (0,0) = top left
		assertArrayEquals(
			new Point3d[] {new Point3d(-0.5,0,0), new Point3d(0,0.5,0)},
			getBoundingBox(faces[0])
		);
		assertEquals(new Color(0,0,0), faceColours[0]);

		// 1 = (0,1) = bottom left
		assertArrayEquals(
			new Point3d[] {new Point3d(-0.5,-0.5,0), new Point3d(0,0,0)},
			getBoundingBox(faces[1])
		);
		assertEquals(new Color(0,1,1), faceColours[1]);

		// 2 = (1,0) = top right
		assertArrayEquals(
			new Point3d[] {new Point3d(0,0,0), new Point3d(0.5,0.5,0)},
			getBoundingBox(faces[2])
		);
		assertEquals(new Color(1,0,1), faceColours[2]);

		// 3 = (1,1) = bottom right
		assertArrayEquals(
			new Point3d[] {new Point3d(0,-0.5,0), new Point3d(0.5,0,0)},
			getBoundingBox(faces[3])
		);
		assertEquals(new Color(1,1,2), faceColours[3]);
	}

	private Point3d[] getBoundingBox(IPolygon3d polygon) {
		if(polygon.size() == 0) throw new RuntimeException();
		Point3d p0 = polygon.get(0);
		double minX = p0.x, maxX = p0.x;
		double minY = p0.y, maxY = p0.y;
		double minZ = p0.z, maxZ = p0.z;
		for(int i=1; i<polygon.size(); i++) {
			Point3d p = polygon.get(i);
			minX = Math.min(p.x, minX);
			minY = Math.min(p.y, minY);
			minZ = Math.min(p.z, minZ);
			maxX = Math.max(p.x, maxX);
			maxY = Math.max(p.y, maxY);
			maxZ = Math.max(p.z, maxZ);
		}
		return new Point3d[] {
			new Point3d(minX, minY, minZ),
			new Point3d(maxX, maxY, maxZ)
		};
	}

	@Test
	void forCoverageSake() {
		new ObjReader();
		new ObjWriter();
		new PolyhedronIO.Helper();
	}

}